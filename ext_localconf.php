<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Ceisufro.ExportGanttCeis', 'Content');

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['cargaDatosGantt'] = 'EXT:export_gantt_ceis/Classes/EID/ServicioDatosGantt.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['respaldoImagenGantt'] = 'EXT:export_gantt_ceis/Classes/EID/GuardadoImagen.php';

