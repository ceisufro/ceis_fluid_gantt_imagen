/**
 * Created by Javier on 02-12-16.
 */
    //default columns definition
gantt.config.columns = [
    {name:"text",       label:"Tarea",  width:300, tree:true },
    {name:"start_date", label:"Inicio", align: "center", width:100 },
    {name:"end_date",   label:"Término",   align: "center", width:100 }
];

gantt.templates.task_text = function(s,e,task){
    return task.text;
};
gantt.config.columns[0].template = function(obj){
    return obj.text;
};

gantt.config.scale_unit = "week";

var demo_tasks = JSON.parse($("#enlaceServicioGantt").val());

gantt.init("gantt_here");
gantt.parse(demo_tasks);