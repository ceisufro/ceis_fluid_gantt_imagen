/**
 * Created by Javier on 28-11-2016.
 */


gantt.exportToPNG({
    callback: function(res){

        var parametros = {
            "url": res.url,
            "nombreArchivo": $("#nombreExportacionImagen").val(),
            "rutaUbicacion": $("#rutaUbicacionImagenGantt").val()
        };

        $.ajax({
            type: "POST",
            //contentType: "application/json; charset=utf-8",
            url: $("#servicioExportaImagen").val(),
            dataType: "json",
            data: parametros,
            success: function (data){ },
            error: function (data){ }
        });
    }});


