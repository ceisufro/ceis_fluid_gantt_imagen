<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 05-12-16
 * Time: 0:15
 */

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

$respuesta = array(
    array(
        "name" => "Sprint 1",
        "desc" => "Tarea 1",
        "values" => [[
            "from" => "2016/02/02",
            "to" => "2016/02/28",
            "label" => "Tarea 1"]]),
    array(
        "name" => "",
        "desc" => "Tarea 2 Tarea 2 Tarea 2",
        "values" => [[
            "from" => "2016/03/01",
            "to" => "2016/03/30",
            "label" => "Tarea 2"]]),
    array(
        "name" => "",
        "desc" => "Tarea 3",
        "values" => [[
            "from" => "2016/04/01",
            "to" => "2016/04/20",
            "label" => "Tarea 3"]]),
    array(
        "name" => "Sprint 2",
        "desc" => "Tarea 4",
        "values" => [[
            "from" => "2016/04/21",
            "to" => "2016/05/15",
            "label" => "Tarea 4"]]),
    array(
        "name" => "",
        "desc" => "Tarea 5",
        "values" => [[
            "from" => "2016/05/16",
            "to" => "2016/06/05",
            "label" => "Tarea 5"]]),
    array(
        "name" => "",
        "desc" => "Tarea 6",
        "values" => [[
            "from" => "2016/06/06",
            "to" => "2016/06/30",
            "label" => "Tarea 6"]]),
    array(
        "name" => "Sprint 3",
        "desc" => "Tarea 7",
        "values" => [[
            "from" => "2016/07/01",
            "to" => "2016/07/25",
            "label" => "Tarea 7"]]),
    array(
        "name" => "",
        "desc" => "Tarea 8",
        "values" => [[
            "from" => "2016/07/26",
            "to" => "2016/08/15",
            "label" => "Tarea 8"]]),
    array(
        "name" => "",
        "desc" => "Tarea 9",
        "values" => [[
            "from" => "2016/08/16",
            "to" => "2016/08/30",
            "label" => "Tarea 9"]]),
    array(
        "name" => "",
        "desc" => "Tarea 10",
        "values" => [[
            "from" => "2016/09/01",
            "to" => "2016/09/30",
            "label" => "Tarea 10"]]),
    array(
        "name" => "",
        "desc" => "Tarea 11",
        "values" => [[
            "from" => "2016/10/01",
            "to" => "2016/12/30",
            "label" => "Tarea 11"]])
);
echo json_encode($respuesta);