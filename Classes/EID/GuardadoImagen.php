<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 05-12-16
 * Time: 14:43
 */

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

$urlImagen = $_POST['url'];
$nombreArchivo = $_POST['nombreArchivo'];
$ubicacionImagen = $_POST['rutaUbicacion'];

$imagen = file_get_contents($urlImagen);

if (!is_dir($ubicacionImagen)) {
    mkdir($ubicacionImagen, 0777, true);
}

if (substr($ubicacionImagen,-1) != "/") {
    $ubicacionImagen = $ubicacionImagen."/";
}

error_log($ubicacionImagen.$nombreArchivo);

if (file_exists($ubicacionImagen.$nombreArchivo)) {
    unlink($ubicacionImagen.$nombreArchivo);
}

error_log("hola");

file_put_contents($ubicacionImagen.$nombreArchivo, $imagen);

echo ("Url recibida: ".$urlImagen. ", nombre archivo: ".$nombreArchivo." y ubicacion de imagen: ".$ubicacionImagen);