<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 02-12-16
 * Time: 16:49
 */

namespace Ceisufro\ExportGanttCeis\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;

class ExportGanttCeisViewHelper extends AbstractFormViewHelper
{
    /**
     * @param string $nombreArchivo
     * @param string $enlaceDatosGantt url completa ej: http://dominio.com/?eID=cargaDatosGantt
     * @param string $enlaceExportaImagen por defecto ?eID=respaldoImagenGantt
     * @param string $rutaGuardado
     * @param array $param
     * @return string
     */
    public function render($nombreArchivo, $enlaceDatosGantt, $enlaceExportaImagen = NULL, $rutaGuardado, $param = NULL)
    {
        $enlaceExportaImagen = $enlaceExportaImagen != NULL ? $enlaceExportaImagen : "?eID=respaldoImagenGantt";
        $directorioVista = PATH_site."typo3conf/ext/export_gantt_ceis/Resources/Private/Templates/VistaGantt/";
        $rutaCompletaVista = $directorioVista.'imagen_gantt.html';
        $vista = new StandaloneView();
        $vista->setFormat("html");
        $vista->setTemplatePathAndFilename($rutaCompletaVista);

        if(empty($enlaceDatosGantt)) {
            $mensaje = "<h3>No se ingreso enlace a servicio de datos </h3>";
        } else {
            $json = $param != NULL ? $this->wsRestRequest("POST", $enlaceDatosGantt, $param) : file_get_contents($enlaceDatosGantt);
            $conenidoJson = json_decode($json,true);

            $arregloTareas = array();
            $contadorId = 1;
            $contadorParent = 1;
            foreach ($conenidoJson as $tareas) {

                $nuevaTarea = array();

                $nuevaTarea['id'] = $contadorId;
                $nuevaTarea["open"] = "true";

                if ($tareas['name'] != "") {
                    $nuevaTarea["text"] = $tareas['name'];
                    $contadorParent = $contadorId;
                } else {
                    $nuevaTarea["text"] = $tareas['desc'];
                    $nuevaTarea["parent"] = $contadorParent;
                }

                if(isset($tareas['values'])){
                    foreach ($tareas['values'] as $valores) {
                        if ($tareas['name'] == "") {
                            //formato de fecha "d-m-Y";
                            $from = $valores['from'];
                            $fechaInicio = str_replace('/', '-', $from);

                            $to = $valores['to'];
                            $fechaTermino = str_replace('/', '-', $to);

                            $nuevaTarea["start_date"] = date('d-m-Y', strtotime($fechaInicio));
                            $nuevaTarea["end_date"]  = date('d-m-Y', strtotime($fechaTermino));
                        }

                    }
                }

                $arregloTareas[] = $nuevaTarea;
                $contadorId = $contadorId + 1;
            }

            $arregloDHTML = array();
            $arregloDHTML["data"] = $arregloTareas;


            $stringJson = json_encode($arregloDHTML);
            $vista->assign("enlaceExportaImagen", $enlaceExportaImagen);
            $vista->assign("nombreArchivo",$nombreArchivo);
            $vista->assign("rutaGuardado",PATH_site.$rutaGuardado);
            $vista->assign("jsonCargaGantt",$stringJson);
        }
        return $vista->render();
    }

    /**
     * Function to call a JSON REST Service
     *
     * @param string $method HTTP method: "GET"(default), "POST", "PUT" or "DELETE"
     * @param string $url dirección servicio
     * @param array $query_params Optional. Associative array containing the variables to use in the request if "POST" or "PUT" method.
     * @return mixed
     */
    private function wsRestRequest($method = "GET", $url, $query_params = NULL) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $method = strtoupper($method);

        if($method == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query_params));
        }elseif($method == "DELETE" || $method == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if($query_params !== NULL) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query_params));
            }
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}